package ds.firstassignment.controller;

import ds.firstassignment.dto.*;
import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.services.CaregiverService;
import ds.firstassignment.services.PatientService;
import ds.firstassignment.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@Controller
@RequestMapping(value = "/caregiver")
public class CaregiverController {


    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private UserService userService;


    @GetMapping(value="/all")
    public ResponseEntity<?> getAllCaregivers() {
        try {
            List<CaregiverDTO> caregivers = caregiverService.findAll();
            return new ResponseEntity<>(caregivers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value="/update")
    public ResponseEntity<?> updateCare(@RequestBody CaregiverWithPatientsDTO caregiverWithPatientsDTO) {
        try {
            CaregiverWithPatientsDTO caregiverDTO = caregiverService.updateCaregiver(caregiverWithPatientsDTO);
            return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(value = "/add")
    public ResponseEntity<?> addCaregiver(@RequestParam Integer doctorId, @RequestBody UserCaregiverDTO newDTO) {
        try {
            caregiverService.addCaregiverAndUserWithDoctor(newDTO.getUserDTO(), newDTO.getCaregiverDTO(), doctorId);
            return new ResponseEntity<>(newDTO.getCaregiverDTO(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteCaregiver(@RequestParam Integer idCaregiver) {
        try {

            caregiverService.deleteCaregiverById(idCaregiver);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value="/updateWithUser")
    public ResponseEntity<?> updateCaregiver(@RequestParam Integer idDoctor,@RequestBody UserCaregiverDTO caregiverDTO) {
        try {
            caregiverService.updateCaregiver(caregiverDTO.getUserDTO(), caregiverDTO.getCaregiverDTO(), idDoctor);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/byDoctor")
    public ResponseEntity<?> getCaregiversByDoctor(@RequestBody DoctorDTO doctorDTO) {
        try {
            List<CaregiverDTO> caregivers = caregiverService.findByDoctor(doctorDTO);
            return new ResponseEntity<>(caregivers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value="/addPacient")
    public ResponseEntity<?> addPacientToCaregiver(@RequestParam Integer caregiverID, @RequestBody PatientWithMedicationDTO patientDTO) {
        try {
            CaregiverWithPatientsDTO caregiver = caregiverService.addPatientToCaregiver(caregiverID, patientDTO);
            return new ResponseEntity<>(caregiver, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/getUser")
    public ResponseEntity<?> getUser(@RequestBody CaregiverDTO caregiverDTO) {
        try {
            UserDTO patient = caregiverService.findUserDTOByCaregiver(caregiverDTO);
            return new ResponseEntity<>(patient, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/profile")
    public ResponseEntity<?> getCaregiverByUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO user = userService.findByUsername(userDTO.getUsername());
            CaregiverDTO caregiverDTO = caregiverService.findByUser(user);
            return new ResponseEntity<>(caregiverDTO , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/getPatients")
    public ResponseEntity<?> getMyPatients(@RequestParam Integer id) {
        try {

            List<PatientDTO> patients = caregiverService.findMyCaregivers(id);
            return new ResponseEntity<>(patients , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
}
