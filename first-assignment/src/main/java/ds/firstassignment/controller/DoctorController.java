package ds.firstassignment.controller;

import ds.firstassignment.dto.*;
import ds.firstassignment.entities.Medication;
import ds.firstassignment.services.CaregiverService;
import ds.firstassignment.services.DoctorService;
import ds.firstassignment.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@Controller
@RequestMapping(value = "/doctor")
public class DoctorController {


    @Autowired
    private DoctorService doctorService;

    @Autowired
    private UserService userService;


    @GetMapping(value = "/all")
    public ResponseEntity<?> getAll() {
        try {
            List<DoctorDTO> doctors = doctorService.findAll();
            return new ResponseEntity<>(doctors, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(value = "/add")
    public ResponseEntity<?> addDoctor(@RequestBody DoctorDTO doctorDTO) {
        try {
            doctorService.addDoctor(doctorDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteDoctor(@RequestBody DoctorDTO doctorDTO) {
        try {
            doctorService.deleteDoctor(doctorDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> updateDoctor(@RequestBody DoctorDTO doctorDTO) {
        try {
            DoctorDTO doctor = doctorService.updateDoctor(doctorDTO);
            return new ResponseEntity<>(doctor, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/get")
    public ResponseEntity<?> getById(@RequestBody Integer id) {
        try {
            DoctorDTO doctor = doctorService.findById(id);
            return new ResponseEntity<>(doctor, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/addCaregiver")
    public ResponseEntity<?> addCaregiverToDoctor(@RequestParam Integer doctorID, @RequestBody CaregiverWithPatientsDTO caregiverDTO) {
        try {
            DoctorDTO doctor = doctorService.addCaregiver(doctorID, caregiverDTO);
            return new ResponseEntity<>(doctor, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/addMedication")
    public ResponseEntity<?> addMedication(@RequestBody MedicationDTO medicationDTO) {
        try {
            doctorService.addMedication(medicationDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/profile")
    public ResponseEntity<?> getCaregiverByUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO user = userService.findByUsername(userDTO.getUsername());
            DoctorDTO doctorDTO = doctorService.findByUser(user);
            return new ResponseEntity<>(doctorDTO , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/getCaregivers")
    public ResponseEntity<?> getCaregiverByUser(@RequestParam Integer id) {
        try {

            List<CaregiverDTO> patients = doctorService.findMyCaregivers(id);
            return new ResponseEntity<>(patients , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

}
