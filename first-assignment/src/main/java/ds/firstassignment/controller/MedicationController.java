package ds.firstassignment.controller;


import ds.firstassignment.dto.MedicationDTO;
import ds.firstassignment.dto.MedicationDTO;
import ds.firstassignment.dto.PatientDTO;
import ds.firstassignment.services.MedicationService;
import ds.firstassignment.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@Controller
@RequestMapping(value = "/medication")
public class MedicationController {

    @Autowired
    private MedicationService medicationService;

    @GetMapping(value="/all")
    public ResponseEntity<?> getAll() {
        try {
            List<MedicationDTO> medications = medicationService.findAll();
            return new ResponseEntity<>(medications, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/add")
    public ResponseEntity<?> addMedication(@RequestBody MedicationDTO medicationDTO) {
        try {
            medicationService.addMedication(medicationDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteMedication(@RequestParam Integer medicationId) {
        try {
            medicationService.deleteMedication(medicationId);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> updateMedication(@RequestBody MedicationDTO medicationDTO) {
        try {
            MedicationDTO medication = medicationService.updateMedication(medicationDTO);
            return new ResponseEntity<>(medication, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }



}
