package ds.firstassignment.controller;


import ds.firstassignment.dto.*;
import ds.firstassignment.entities.Patient;
import ds.firstassignment.entities.User;
import ds.firstassignment.services.PatientService;
import ds.firstassignment.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@Controller
@RequestMapping(value = "/patient")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private UserService userService;

    @GetMapping(value="/all")
    public ResponseEntity<?> getAllPatients() {
        try {
            List<PatientDTO> patients = patientService.findAll();
            return new ResponseEntity<>(patients, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/add")
    public ResponseEntity<?> addPatient(@RequestParam Integer caregiverId, @RequestBody UserPatientDTO newDTO) {
        try {
            patientService.addPatientAndUserWithCaregiver(newDTO.getUserDTO(), newDTO.getPatientDTO(), caregiverId);
            return new ResponseEntity<>(newDTO.getPatientDTO(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deletePatient(@RequestParam Integer patientId) {
        try {
            PatientDTO patient = patientService.findById(patientId);
            patientService.deletePatient(patient);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value="/update")
    public ResponseEntity<?> updatePatient(@RequestParam Integer caregiverId,@RequestBody UserPatientDTO patientDTO) {
        try {
             patientService.updatePatient(patientDTO.getUserDTO(),patientDTO.getPatientDTO(), caregiverId);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/myIntakes")
    public ResponseEntity<?> getMyIntakes(@RequestParam Integer idPatient) {
        try {
            List<IntakeDTO> intakes = this.patientService.getMyIntakes(idPatient);
            return new ResponseEntity<>(intakes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/byPatient")
    public ResponseEntity<?> getPatientsByCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        try {
            List<PatientDTO> intakes = patientService.findByCaregiver(caregiverDTO);
            return new ResponseEntity<>(intakes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value="/addMedication")
    public ResponseEntity<?> addMedicationToPatient(@RequestParam Integer patientID, @RequestBody IntakeWithMedicationDTO intakeDTO) {
        try {
            PatientWithMedicationDTO patient = patientService.addMedication(patientID, intakeDTO);
            return new ResponseEntity<>(patient, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/getUser")
    public ResponseEntity<?> addMedicationToPatient(@RequestBody PatientDTO patientDTO) {
        try {
            UserDTO patient = patientService.findUserDTOByPatient(patientDTO);
            return new ResponseEntity<>(patient, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/profile")
    public ResponseEntity<?> getPatientByUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO user = userService.findByUsername(userDTO.getUsername());
            PatientDTO caregiverDTO = patientService.findByUser(user);
            return new ResponseEntity<>(caregiverDTO , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

}
