package ds.firstassignment.dto;

import java.sql.Date;
import java.util.List;

public class CaregiverWithPatientsDTO {

    private int id;
    private String name;
    private Date birthDate;
    private String address;
    private String gender;
    private List<PatientWithMedicationDTO> patients;

    public CaregiverWithPatientsDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PatientWithMedicationDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientWithMedicationDTO> patients) {
        this.patients = patients;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
