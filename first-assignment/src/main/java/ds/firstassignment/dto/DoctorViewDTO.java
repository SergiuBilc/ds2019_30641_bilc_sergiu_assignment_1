package ds.firstassignment.dto;

import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.User;

import java.util.List;

public class DoctorViewDTO {

    private int id;
    private String name;
    private String address;
    private List<CaregiverDTO> caregivers;

    public DoctorViewDTO() {
    }

    public DoctorViewDTO(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<CaregiverDTO> getCaregivers() {
        return caregivers;
    }

    public void setCaregivers(List<CaregiverDTO> caregivers) {
        this.caregivers = caregivers;
    }
}
