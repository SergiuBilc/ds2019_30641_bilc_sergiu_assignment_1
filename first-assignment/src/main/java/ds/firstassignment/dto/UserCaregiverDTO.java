package ds.firstassignment.dto;

public class UserCaregiverDTO {

    private CaregiverDTO caregiverDTO;
    private UserDTO userDTO;

    public UserCaregiverDTO() {
    }


    public UserCaregiverDTO(CaregiverDTO caregiverDTO, UserDTO userDTO) {
        this.caregiverDTO = caregiverDTO;
        this.userDTO = userDTO;
    }

    public CaregiverDTO getCaregiverDTO() {
        return caregiverDTO;
    }

    public void setCaregiverDTO(CaregiverDTO caregiverDTO) {
        this.caregiverDTO = caregiverDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
