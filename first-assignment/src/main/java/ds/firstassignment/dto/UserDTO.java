package ds.firstassignment.dto;

import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.Role;

import java.util.List;

public class UserDTO {

    private int id;
    private String username;
    private String password;
    private String role;

    public UserDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
