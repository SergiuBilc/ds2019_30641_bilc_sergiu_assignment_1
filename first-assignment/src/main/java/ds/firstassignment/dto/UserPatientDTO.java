package ds.firstassignment.dto;

public class UserPatientDTO {

    private PatientDTO patientDTO;
    private UserDTO userDTO;

    public UserPatientDTO() {
    }

    public UserPatientDTO(PatientDTO patientDTO, UserDTO userDTO) {
        this.patientDTO = patientDTO;
        this.userDTO = userDTO;
    }

    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}