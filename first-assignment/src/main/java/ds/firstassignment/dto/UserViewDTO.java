package ds.firstassignment.dto;

import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.Role;
import ds.firstassignment.entities.User;

import java.util.List;

public class UserViewDTO {

    private String name;
    private String password;
    private Role role;

    public UserViewDTO() {
    }

    public UserViewDTO(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
