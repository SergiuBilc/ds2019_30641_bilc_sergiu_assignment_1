package ds.firstassignment.dto.mapper;

import ds.firstassignment.dto.CaregiverDTO;
import ds.firstassignment.dto.DoctorDTO;
import ds.firstassignment.dto.DoctorViewDTO;
import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.Doctor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoctorMapper {

    Doctor fromDoctorDTOtoDoctorEntity(DoctorDTO doctorDto);

    DoctorDTO fromEntityToDoctorDTO(Doctor doctor);

    List<Doctor> fromDoctorDTOtoDoctorEntityList(List<DoctorDTO> doctorDto);

    List<DoctorDTO> fromEntityToDoctorDTOList(List<Doctor> doctor);

    Doctor fromDoctorViewDTOtoDoctorEntity(DoctorViewDTO doctorDto);

    DoctorViewDTO fromEntityToDoctorViewDTO(Doctor doctor);

}
