package ds.firstassignment.dto.mapper;

import ds.firstassignment.dto.IntakeDTO;
import ds.firstassignment.dto.IntakeWithMedicationDTO;
import ds.firstassignment.entities.Intake;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IntakeMapper {
    Intake intakeDTOtoIntake(IntakeDTO intakeDTO);

    IntakeDTO intakeToIntakeDTO(Intake intake);

    List<IntakeDTO> intakeListtoIntakeDTOList(List<Intake> intakes);

    List<Intake> intakeDTOListoIntakeList(List<IntakeDTO> intakeDTOS);

    Intake intakeMedicationDTOtoIntake(IntakeWithMedicationDTO intakeDTO);

    IntakeWithMedicationDTO intakeToIntakeMedicationDTO(Intake intake);

    List<Intake> intakeMedicationDTOtoIntakeList(List<IntakeWithMedicationDTO> medicationPlans);
}
