package ds.firstassignment.dto.mapper;

import ds.firstassignment.dto.PatientDTO;
import ds.firstassignment.dto.PatientViewDTO;
import ds.firstassignment.dto.PatientWithMedicationDTO;
import ds.firstassignment.entities.Patient;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PatientMapper {

    Patient patientDTOtoPatient(PatientDTO patientDTO);

    PatientDTO patientToPatientDTO(Patient patient);

    List<PatientDTO> patientListtoPatientDTOList(List<Patient> patients);

    List<Patient> patientDTOListtoPatientList(List<PatientDTO> patientDTOS);

    Patient patientWithMedicationDTOtoPatient(PatientWithMedicationDTO patientWithMedicationDTO);

    PatientWithMedicationDTO patientToPatientWithMedicationDTO(Patient patient);

    List<Patient> patientWithMedicationDTOtoPatientList(List<PatientWithMedicationDTO> patients);
}
