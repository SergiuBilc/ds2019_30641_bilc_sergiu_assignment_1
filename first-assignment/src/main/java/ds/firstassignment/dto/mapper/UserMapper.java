package ds.firstassignment.dto.mapper;

import ds.firstassignment.dto.DoctorDTO;
import ds.firstassignment.dto.DoctorViewDTO;
import ds.firstassignment.dto.UserDTO;
import ds.firstassignment.dto.UserViewDTO;
import ds.firstassignment.entities.Doctor;
import ds.firstassignment.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ValueMapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User fromUserDTOtoUserEntity(UserDTO doctorDto);

    UserDTO fromEntityToUserDTO(User user);

    List<User> fromUserDTOToEntityList(List<UserDTO> userDtos);

    List<UserDTO> fromEntityToUserDTOList(List<User> user);
//
//    User fromUserViewDTOtoUserEntity(UserViewDTO doctorDto);
//
//    UserViewDTO fromEntityToUserViewDTO(User user);
}
