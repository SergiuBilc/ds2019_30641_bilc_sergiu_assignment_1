package ds.firstassignment.repositories;

import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.Doctor;
import ds.firstassignment.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

    public List<Caregiver> findByDoctor(Doctor doctor);


    public Caregiver findByUser(User user);
}
