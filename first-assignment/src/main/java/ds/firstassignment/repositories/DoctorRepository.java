package ds.firstassignment.repositories;

import ds.firstassignment.entities.Doctor;
import ds.firstassignment.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository  extends JpaRepository<Doctor, Integer> {

    public Doctor findByUser(User user);
}
