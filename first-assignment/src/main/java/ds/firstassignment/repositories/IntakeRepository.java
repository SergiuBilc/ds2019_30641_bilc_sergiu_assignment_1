package ds.firstassignment.repositories;

import ds.firstassignment.entities.Intake;
import ds.firstassignment.entities.Medication;
import ds.firstassignment.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IntakeRepository extends JpaRepository<Intake, Integer> {

    public List<Intake> findByPatient(Patient patient);

    public Intake findByMedication(Medication medication);

}
