package ds.firstassignment.repositories;

import ds.firstassignment.entities.Intake;
import ds.firstassignment.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Integer> {

}
