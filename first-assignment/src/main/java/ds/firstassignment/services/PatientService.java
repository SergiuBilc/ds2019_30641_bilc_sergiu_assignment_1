package ds.firstassignment.services;

import ds.firstassignment.dto.*;
import ds.firstassignment.dto.mapper.CaregiverMapper;
import ds.firstassignment.dto.mapper.IntakeMapper;
import ds.firstassignment.dto.mapper.PatientMapper;
import ds.firstassignment.dto.mapper.UserMapper;
import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.Intake;
import ds.firstassignment.entities.Patient;
import ds.firstassignment.entities.User;
import ds.firstassignment.repositories.CaregiverRepository;
import ds.firstassignment.repositories.PatientRepository;
import ds.firstassignment.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private CaregiverMapper caregiverMapper;

    @Autowired
    private IntakeMapper intakeMapper;

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRepository userRepository;

    public void addPatient(PatientDTO patientDTO) {
        Patient patient = this.patientMapper.patientDTOtoPatient(patientDTO);
        this.patientRepository.save(patient);
    }

    public PatientDTO updatePatient(PatientWithMedicationDTO patientDTO) {
        Patient patient = this.patientRepository.findById(patientDTO.getId()).get();

        if (patient != null) {
            patient.setName(patientDTO.getName());
            patient.setAddress(patientDTO.getAddress());
            patient.setBirthDate(patientDTO.getBirthDate());
            patient.setGender(patientDTO.getGender());
            patient.setMedicalRecord(patientDTO.getMedicalRecord());

            if (patientDTO.getMedicationPlans() != null) {
                patient.setIntakes(this.intakeMapper.intakeMedicationDTOtoIntakeList(patientDTO.getMedicationPlans()));
            }
        }

        return this.patientMapper.patientToPatientDTO(patientRepository.save(patient));
    }

    public PatientWithMedicationDTO addMedication(Integer patientID, IntakeWithMedicationDTO intakeDTO) {
        Patient patient = this.patientRepository.findById(patientID).get();
        Intake intake = this.intakeMapper.intakeMedicationDTOtoIntake(intakeDTO);
        patient.getIntakes().add(intake);
        this.patientRepository.save(patient);
        return this.patientMapper.patientToPatientWithMedicationDTO(patient);

    }

    public void deletePatient(PatientDTO patientDTO) {
        Patient patient = this.patientRepository.findById(patientDTO.getId()).get();
        this.userRepository.delete(patient.getUser());
        this.patientRepository.delete(patient);
    }

    public PatientDTO findById(Integer id) {
        return this.patientMapper.patientToPatientDTO(this.patientRepository.findById(id).get());
    }

    public List<PatientDTO> findAll() {
        return this.patientMapper.patientListtoPatientDTOList(this.patientRepository.findAll());
    }

    public List<PatientDTO> findByCaregiver(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = this.caregiverMapper.fromCaregiverDTOtoCaregiverEntity(caregiverDTO);

        return this.patientMapper.patientListtoPatientDTOList(this.patientRepository.findByCaregiver(caregiver));
    }

    public User findUserByPatient(PatientDTO patientDTO) {
        Patient patient = this.patientRepository.findById(patientDTO.getId()).get();
        return patient.getUser();
    }

    public UserDTO findUserDTOByPatient(PatientDTO patientDTO) {
        Patient patient = this.patientRepository.findById(patientDTO.getId()).get();

        return this.userMapper.fromEntityToUserDTO(patient.getUser());
    }

    public void addPatientAndUserWithCaregiver(UserDTO userDTO, PatientDTO patientDTO, Integer idCaregiver) {

        Patient patient = getPatient(patientDTO);
        Caregiver caregiver = caregiverRepository.findById(idCaregiver).get();
        User user = new User(userDTO.getUsername(), userDTO.getPassword(), userDTO.getRole());
        patient.setUser(user);
        patient.setCaregiver(caregiver);

        patientRepository.save(patient);
    }

    public void updatePatient(UserDTO userDTO, PatientDTO patientDTO, Integer caregiverId) {
        Patient patient = patientRepository.findById(patientDTO.getId()).get();
        User user = this.findUserByPatient(patientDTO);

        patient = updateFieldsPatient(patient,patientDTO);
        updateUserFields(userDTO, user);
        patient.setUser(user);

        Caregiver caregiver = this.caregiverRepository.findById(caregiverId).get();
        if(caregiver!=null) {
            patient.setCaregiver(caregiver);
        }

        userRepository.save(user);
        patientRepository.save(patient);
    }


    private Patient getPatient(PatientDTO patientDTO) {
        Patient patient = new Patient();
        updateFieldsPatient(patient, patientDTO);
        return patient;
    }

    private Patient updateFieldsPatient(Patient patient, PatientDTO patientDTO) {
        patient.setName(patientDTO.getName());
        patient.setAddress(patientDTO.getAddress());
        patient.setBirthDate(patientDTO.getBirthDate());
        patient.setGender(patientDTO.getGender());
        patient.setMedicalRecord(patientDTO.getMedicalRecord());
        return patient;
    }

    private void updateUserFields(UserDTO userDTO, User user) {
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
    }

    public PatientDTO findByUser(UserDTO user) {
        return patientMapper.patientToPatientDTO(patientRepository.findByUser(userMapper.fromUserDTOtoUserEntity(user)));
    }

    public List<IntakeDTO> getMyIntakes(Integer idPatient) {
        Patient patient = this.patientRepository.findById(idPatient).get();
        List<Intake> intakes = patient.getIntakes();
        return this.intakeMapper.intakeListtoIntakeDTOList(intakes);
    }
}