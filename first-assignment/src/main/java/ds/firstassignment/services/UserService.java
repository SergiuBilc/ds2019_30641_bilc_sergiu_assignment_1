package ds.firstassignment.services;

import ds.firstassignment.dto.UserDTO;
import ds.firstassignment.dto.mapper.UserMapper;
import ds.firstassignment.entities.Doctor;
import ds.firstassignment.entities.User;
import ds.firstassignment.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public void addUser(User user){
        userMapper.fromEntityToUserDTO(new User());
        this.userRepository.save(user);
    }

    public UserDTO findByUsername(String username){
        User user = this.userRepository.findByUsername(username);
        return this.userMapper.fromEntityToUserDTO(user);
    }

    public UserDTO updateUser(User user){
        return  this.userMapper.fromEntityToUserDTO(this.userRepository.save(user));
    }

    public void deleteUser(User user){
        this.userRepository.delete(user);
    }

    public UserDTO findById(Integer id){
        return this.userMapper.fromEntityToUserDTO(this.userRepository.findById(id).get());
    }

    public User findEntityById(Integer id){
        return this.userRepository.findById(id).get();
    }

    public List<UserDTO> findAll(){
        return  this.userMapper.fromEntityToUserDTOList(this.userRepository.findAll());
    }

}
