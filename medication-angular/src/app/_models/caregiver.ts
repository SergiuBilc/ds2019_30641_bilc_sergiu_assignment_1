import { Pacient } from './pacient';
import { Doctor } from './doctor';

export class Caregiver {
    id: number;
    name: string;
    birthDate: Date;
    address: string;
    gender: String;
    doctor: Doctor;
    patients?: Pacient[];


    constructor(id: number,
        name: string,
        birthDate: Date,
        gender: string,
        address: string,) {

        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address
    }
}