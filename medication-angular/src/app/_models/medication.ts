export class Medication{
    id: number;
    name: string;
    description: string;

    constructor(id: number, name: string, description: string){
        this.description = description;
        this.name = name;
        this.id = id;
    }
}