import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Caregiver } from '../_models/caregiver';
import { User } from '../_models/user';
import { HttpClient } from '@angular/common/http';
import { Pacient } from '../_models/pacient';

@Injectable({ providedIn: 'root' })
export class CaregiverService {
   
    constructor(private http: HttpClient) { }

    getMyProfile(user: User) {
        return this.http.post<Caregiver>(`caregiver/profile`, user);
    }

    getAll(): Observable<Caregiver[]> {
        return this.http.get<Caregiver[]>(`caregiver/all`);
    }

    getById(id: number) {
        return this.http.get<Caregiver>(`caregiver/${id}`);
    }

    updateCaregiver(user: User, caregiver: Caregiver, doctorId: number) {
        let newDTO: { userDTO: User, caregiverDTO: Caregiver } = { userDTO: user, caregiverDTO: caregiver };
        console.log(newDTO);
        return this.http.put<Caregiver>('caregiver/updateWithUser', newDTO, { params: { idDoctor: doctorId + '' } });
    }

    addCaregiver(user: User, caregiver: Caregiver, doctorId: number) {
        let newDTO: { userDTO: User, caregiverDTO: Caregiver } = { userDTO: user, caregiverDTO: caregiver };
        return this.http.post<any>('caregiver/add', newDTO, { params: { doctorId: doctorId + '' } });
    }

    deleteCaregiver(id: number) {
        return this.http.delete<number>('caregiver/delete', { params: { idCaregiver: id + '' } });
    }

    getUser(caregiver: Caregiver) {
        return this.http.post<User>('caregiver/getUser', caregiver);
    }

    getMyPatients(id: number){
        return this.http.get<Pacient[]>(`caregiver/getPatients`, { params: { id: id+''}});
    }

}