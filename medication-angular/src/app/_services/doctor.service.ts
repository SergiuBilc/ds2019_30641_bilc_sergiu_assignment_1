import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../_models/user';
import { HttpClient } from '@angular/common/http';
import { Doctor } from '../_models/doctor';
import { Pacient } from '../_models/pacient';
import { Caregiver } from '../_models/caregiver';

@Injectable({ providedIn: 'root' })
export class DoctorService {
    constructor(private http: HttpClient) { }

    getMyProfile(user: User){
        return this.http.post<Doctor>(`doctor/profile`, user);
    }

    getAll(): Observable<Doctor[]> {
        return this.http.get<Doctor[]>(`doctor/all`);
    }

    getById(id: number) {
        return this.http.get<Doctor>(`doctor/${id}`);
    }

    getMyPatients(id: number){
        return this.http.get<Pacient[]>(`doctor/getPatients`, { params: { id: id+''}});
    }

    getMyCaregivers(id: number){
        return this.http.get<Caregiver[]>(`doctor/getCaregivers`, { params: { id: id+''}});
    }

    updateCaregiver(patient: Doctor) {
        return this.http.put<Doctor>('doctor/update', patient);
    }

    addCaregiver(user: User, caregiver: Doctor, doctorId: number) {
        let newDTO: { userDTO: User, patientDTO: Doctor } = { userDTO: user, patientDTO: caregiver };
        return this.http.post<any>('doctor/add', newDTO, { params: { doctorId: doctorId+''} });
    }

    deleteCaregiver(id: number) {
        return this.http.delete<number>('doctor/delete', {params: {patientId: id+''}});
    }

}