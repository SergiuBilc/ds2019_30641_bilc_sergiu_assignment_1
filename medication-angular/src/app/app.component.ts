import { Component, OnInit } from '@angular/core';
import { User } from './_models/user';
import { Router } from '@angular/router';
import { AuthenticationService } from './_services/authentication.service';
import { Role } from './_models/role';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  currentUser: User;

  constructor(private router: Router,
    private authenticationService: AuthenticationService){
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
 
  }

  ngOnInit(){
     }

  get isDoctor(){
    return this.currentUser && this.currentUser.role === Role.Doctor;
  }

  
  get isCaregiver(){
    return this.currentUser && this.currentUser.role === Role.Caregiver;
  }

  
  get isPatient(){
    return this.currentUser && this.currentUser.role === Role.Patient;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
}
}
