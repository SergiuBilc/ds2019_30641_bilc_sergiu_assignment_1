import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatSortModule, MatTableModule, MatDialogModule, MatButtonModule, MatCardModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { CaregiverEditComponent } from './doctor/caregivers/caregiver-edit/caregiver-edit.component';
import { CrudCaregiversComponent } from './doctor/caregivers/crud-caregivers/crud-caregivers.component';
import { DoctorDetailComponent } from './doctor/doctor-detail/doctor-detail.component';
import { DoctorComponent } from './doctor/doctor.component';
import { PacientEditComponent } from './doctor/patients/pacient-edit/pacient-edit.component';
import { PacientsTableComponent } from './doctor/patients/pacients-table/pacients-table.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PatientComponent } from './patient/patient.component';
import { AuthGuard } from './_guards/auth.guards';
import { APIInterceptor } from './_helpers/api.interceptor';
import { AuthenticationService } from './_services/authentication.service';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import { MedicationListComponent } from './doctor/medications/medication-list/medication-list.component';
import { MedicationEditComponent } from './doctor/medications/medication-edit/medication-edit.component';
import { MyCaregiversComponent } from './doctor/my-caregivers/my-caregivers.component';
import { MyPatientsComponent } from './caregiver/my-patients/my-patients.component';
import { MedicationPlanComponent } from './patient/medication-plan/medication-plan.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DoctorComponent,
    CaregiverComponent,
    PatientComponent,
    LoginComponent,
    PacientsTableComponent,
    PacientEditComponent,
    CrudCaregiversComponent,
    DoctorDetailComponent,
    CaregiverEditComponent,
    ConfirmationDialogComponent,
    MedicationListComponent,
    MedicationEditComponent,
    MyCaregiversComponent,
    MyPatientsComponent,
    MedicationPlanComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatSortModule,
    NgbModule,
    ModalModule.forRoot(),
    NgbModule,
    routing
  ],
  entryComponents: [PacientEditComponent, CaregiverEditComponent, ConfirmationDialogComponent, MedicationEditComponent, MedicationPlanComponent],
  providers: [AuthenticationService, AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: APIInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
