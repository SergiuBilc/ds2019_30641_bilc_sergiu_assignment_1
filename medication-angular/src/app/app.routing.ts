import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DoctorComponent } from './doctor/doctor.component';
import { Role } from './_models/role';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards/auth.guards';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { PatientComponent } from './patient/patient.component';
import { PacientsTableComponent } from './doctor/patients/pacients-table/pacients-table.component';
import { CrudCaregiversComponent } from './doctor/caregivers/crud-caregivers/crud-caregivers.component';
import { DoctorDetailComponent } from './doctor/doctor-detail/doctor-detail.component';
import { MedicationListComponent } from './doctor/medications/medication-list/medication-list.component';

const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'doctor',
        component: DoctorComponent, children: [
            { path: '', component: DoctorDetailComponent},
            { path: 'caregivers', component: CrudCaregiversComponent},
            { path: 'patients', component: PacientsTableComponent},
            { path: 'medications', component: MedicationListComponent},
        ],
        canActivate: [AuthGuard],
        data: { roles: [Role.Doctor] }
    },
    {
        path: 'caregiver',
        component: CaregiverComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Caregiver],}
    },
    {
        path: 'patient',
        component: PatientComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Patient] }
    },
    {
        path: 'login',
        component: LoginComponent
    },
];

export const routing = RouterModule.forRoot(appRoutes);