import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Pacient } from 'src/app/_models/pacient';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PatientService } from 'src/app/_services/patient.service';
import { PacientEditComponent } from 'src/app/doctor/patients/pacient-edit/pacient-edit.component';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { Doctor } from 'src/app/_models/doctor';
import { CaregiverService } from 'src/app/_services/caregiver.service';
import { Medication } from 'src/app/_models/medication';
import { MedicationPlanComponent } from 'src/app/patient/medication-plan/medication-plan.component';

@Component({
  selector: 'app-my-patients',
  templateUrl: './my-patients.component.html',
  styleUrls: ['./my-patients.component.css']
})
export class MyPatientsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'gender', 'address', 'medical record', 'caregiver', 'medication plan'];
  dataSource: MatTableDataSource<Pacient>;
  pacients: Pacient[] = [];
  bsModalRef: BsModalRef;


  @Input('doctor') currentDoctor: Doctor;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private caregiverService: CaregiverService,
    private modalService: BsModalService,
    public dialog: MatDialog, ) {


  }


  ngOnInit() {
    this.dataSource = new MatTableDataSource([]);
    this.fetchData();
  }

  private fetchData() {
    this.caregiverService.getMyPatients(this.currentDoctor.id).subscribe(data => {
      this.pacients = data;
      console.log(this.pacients);
      this.dataSource.data = this.pacients;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onOpenMedicationPlan(row) {

    const initialState = {
      currentPatient : row,
      openInModal: true
    };

    this.bsModalRef = this.modalService.show(MedicationPlanComponent, { initialState });
   // this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());

  }
}
