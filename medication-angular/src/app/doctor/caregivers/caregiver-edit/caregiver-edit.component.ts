import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Caregiver } from 'src/app/_models/caregiver';
import { Doctor } from 'src/app/_models/doctor';
import { FormGroup, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CaregiverService } from 'src/app/_services/caregiver.service';
import { DoctorService } from 'src/app/_services/doctor.service';
import { User } from 'src/app/_models/user';
import { Role } from 'src/app/_models/role';

@Component({
  selector: 'app-caregiver-edit',
  templateUrl: './caregiver-edit.component.html',
  styleUrls: ['./caregiver-edit.component.css']
})
export class CaregiverEditComponent implements OnInit, OnDestroy {

  username: string = '';
  password: string = '';
  caregiver: Caregiver;
  doctors: Doctor[];
  user: User;
  isFormReady: boolean = false;

  @Output() addEmitter = new EventEmitter<any>();

  myGroup: FormGroup;

  editCaregiver: boolean = true;

  constructor(public bsModalRef: BsModalRef,
    private caregiverService: CaregiverService,
    private doctorService: DoctorService) {

  }

  ngOnInit() {
    if (!this.editCaregiver) {
      this.doctorService.getAll().subscribe(
        (data) => {
          this.doctors = data;
          this.myGroup = new FormGroup({
            'username': new FormControl(this.username),
            'password': new FormControl(this.password),
            'gender': new FormControl(this.caregiver.gender),
            'name': new FormControl(this.caregiver.name),
            'address': new FormControl(this.caregiver.address),
            'birthDate': new FormControl(this.caregiver.birthDate),
            'doctors': new FormControl(['']),
          });
          this.isFormReady = true;
          let doctorID = this.doctors.findIndex(value => value.id == this.caregiver.doctor.id);

          this.myGroup.controls.doctors.patchValue(this.doctors[doctorID].id);

        }
      );
    }

    if (this.editCaregiver) {
      this.caregiverService.getUser(this.caregiver).subscribe(
        (response) => {
          this.user = response;
          this.username = this.user.username;
          this.password = this.user.password;
          this.myGroup = new FormGroup({
            'username': new FormControl(this.username),
            'password': new FormControl(this.password),
            'gender': new FormControl(this.caregiver.gender),
            'name': new FormControl(this.caregiver.name),
            'address': new FormControl(this.caregiver.address),
            'birthDate': new FormControl(this.caregiver.birthDate),
            'doctors': new FormControl(['']),
          });
          this.isFormReady = true;
          console.log(this.caregiver);
          let doctorID = this.doctors.findIndex(value => value.id == this.caregiver.doctor.id);
          this.myGroup.controls.doctors.patchValue(this.doctors[doctorID].id);

        }
      );

    }
  }

  ngOnDestroy() {
    this.isFormReady = false;
  }

  handleSubmit() {
    if (this.editCaregiver) {
      let caregiver: Caregiver = this.computeCaregiver();
      let user: User = this.computeUser();

      this.caregiverService.updateCaregiver(user, caregiver, this.myGroup.value.doctors).subscribe(
        res => {
          console.log(res);
          this.addEmitter.emit();
          this.bsModalRef.hide();
        }
      );
    } else {

      let user: User = this.computeUser();
      let caregiver: Caregiver = this.computeCaregiver();
      this.caregiverService.addCaregiver(user, caregiver, this.myGroup.value.doctors).subscribe(
        res => {
          console.log(res);
          this.addEmitter.emit();
          this.bsModalRef.hide();
        }
      );
    }
  }


  private computeUser(): User {
    console.log(this.editCaregiver)
    if (this.editCaregiver) {
      // console.log(this.user.id)
      return new User(this.user.id, this.myGroup.value.username, this.myGroup.value.password, Role.Caregiver);
    } else {
      return new User(null, this.myGroup.value.username, this.myGroup.value.password, Role.Caregiver);
    }
  }

  private computeCaregiver(): Caregiver {
    if (this.editCaregiver) {
      // console.log(this.user.id);
      return new Caregiver(this.caregiver.id, this.myGroup.value.name, this.myGroup.value.birthDate, this.myGroup.value.gender, this.myGroup.value.address);
    } else {
      return new Caregiver(null, this.myGroup.value.name, this.myGroup.value.birthDate, this.myGroup.value.gender, this.myGroup.value.address);

    }
  }



}
