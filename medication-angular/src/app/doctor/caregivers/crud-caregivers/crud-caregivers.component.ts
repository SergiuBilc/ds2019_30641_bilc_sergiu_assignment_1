import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Caregiver } from 'src/app/_models/caregiver';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CaregiverService } from 'src/app/_services/caregiver.service';
import { CaregiverEditComponent } from '../caregiver-edit/caregiver-edit.component';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-crud-caregivers',
  templateUrl: './crud-caregivers.component.html',
  styleUrls: ['./crud-caregivers.component.css']
})
export class CrudCaregiversComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'birthDate','gender', 'address', 'doctor', 'actions'];
  dataSource: MatTableDataSource<Caregiver>;
  caregivers: Caregiver[] = [];
  bsModalRef: BsModalRef;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private caregiverService: CaregiverService,
    private modalService: BsModalService,
    public dialog: MatDialog,
  ) {

  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource([]);

    this.fetchData();
  }

  private fetchData() {
    this.caregiverService.getAll().subscribe(data => {
      this.caregivers = data;
      console.log("caregivers: " + this.caregivers);
      this.dataSource.data = this.caregivers;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onUpdateCaregiver(row) {
    const initialState = {
      username: '',
      password: '',
      caregiver: this.caregivers.find((caregiver) => caregiver.id === row.id),
      editCaregiver: true
    };

    this.bsModalRef = this.modalService.show(CaregiverEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  
  }

  onAddCaregiver() {
    const initialState = {
      username: '',
      password: '',
      caregiver: {},
      editCaregiver: false
    };

    this.bsModalRef = this.modalService.show(CaregiverEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  }

  onDeleteRow(row) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, { width: '350px', data: 'Are you sure you want to delete ' + row.name + '?' });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          console.log('User confirmed:', result),
            this.caregiverService.deleteCaregiver(row.id).subscribe(res =>{ this.fetchData()});

        } else {
          console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)');
        }
      }
    );
  }
}
