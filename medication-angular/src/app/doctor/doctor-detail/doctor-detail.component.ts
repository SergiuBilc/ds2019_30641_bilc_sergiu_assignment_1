import { Component, OnInit, OnDestroy } from '@angular/core';
import { Doctor } from 'src/app/_models/doctor';
import { DoctorService } from 'src/app/_services/doctor.service';
import { User } from 'src/app/_models/user';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.css']
})
export class DoctorDetailComponent implements OnInit {

  private readyToRender: boolean = false;
  private currentDoctorSubject: BehaviorSubject<Doctor>;
  public doctor: Observable<Doctor>;

  getDoctorEmitter: Observable<Doctor>;

  constructor(private doctorService: DoctorService, ) {
    this.currentDoctorSubject = new BehaviorSubject<Doctor>(new Doctor());
    this.doctor = this.currentDoctorSubject.asObservable();
  }
  ngOnInit() {
    this.computeDoctor();
  }

  public get currentUserValue(): Doctor {
    return this.currentDoctorSubject.value;
  }

  computeDoctor() {
    let user: User = JSON.parse(localStorage.getItem('currentUser'));
    console.log("Current doctor: " + user);
    this.doctorService.getMyProfile(user).subscribe(res => {
      this.currentDoctorSubject.next(res);
      this.readyToRender = true;
    });
  }

}
