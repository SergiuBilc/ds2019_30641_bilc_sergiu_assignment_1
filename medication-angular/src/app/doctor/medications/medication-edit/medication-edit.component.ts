import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Medication } from 'src/app/_models/medication';
import { FormGroup, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MedicationService } from 'src/app/_services/medication.service';

@Component({
  selector: 'app-medication-edit',
  templateUrl: './medication-edit.component.html',
  styleUrls: ['./medication-edit.component.css']
})
export class MedicationEditComponent implements OnInit {


  medication: Medication;


  myGroup: FormGroup;

  isFormReady: boolean = false;


  @Output() addEmitter = new EventEmitter<any>();

  editMedication: boolean = true;

  constructor(public bsModalRef: BsModalRef, private medicationService: MedicationService,) {  }

  ngOnInit() {

    this.myGroup = new FormGroup({
      name: new FormControl(this.medication.name),
      description: new FormControl(this.medication.description),
    });

    this.isFormReady = true;
  }


  handleSubmit() {
    if (this.editMedication) {
      let medication: Medication = this.computeMedication();

      this.medicationService.updateMedication(medication).subscribe(
        res => {
          console.log(res);
          this.addEmitter.emit();
          this.bsModalRef.hide();
        }
      );
    } else {
      let medication: Medication = this.computeMedication();
      this.medicationService.addMedication(medication).subscribe(
        res => {
          console.log(res);
          this.addEmitter.emit();
          this.bsModalRef.hide();
        }
      );
    }
  }


  private computeMedication(): Medication {
    if (this.editMedication) {
      return new Medication(this.medication.id, this.myGroup.value.name, this.myGroup.value.description);
    } else {
      return new Medication(null, this.myGroup.value.name, this.myGroup.value.description);
    }
  }

}
