import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Medication } from 'src/app/_models/medication';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MedicationService } from 'src/app/_services/medication.service';
import { MedicationEditComponent } from '../medication-edit/medication-edit.component';

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent implements OnInit {

  
  displayedColumns: string[] = ['id', 'name', 'description', 'actions'];
  dataSource: MatTableDataSource<Medication>;
  medications: Medication[] = [];
  bsModalRef: BsModalRef;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private medicationService: MedicationService,
    private modalService: BsModalService,
    public dialog: MatDialog,) {

    this.fetchData();
  }

  private fetchData() {
    this.medicationService.getAll().subscribe(data => {
      this.medications = data;
      this.dataSource.data = this.medications;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource([]);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onUpdateMedication(row) {
    
    const initialState = {
      medication: this.medications.find((pacient) => pacient.id === row.id),
      editMedication: true
    };

    this.bsModalRef = this.modalService.show(MedicationEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  
  }

  onAddPatient(){
    
    const initialState = {
      medication:{},
      editMedication: false
    };

    this.bsModalRef = this.modalService.show(MedicationEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  }


  onDeleteRow(row) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent,{width:'350px', data: 'Are you sure you want to delete '+ row.name+'?'});

    dialogRef.afterClosed().subscribe(
      result => {
        if(result){
          console.log('User confirmed:', result),
          this.medicationService.deleteMedication(row.id).subscribe(res=>{
            this.fetchData();
          });
    
        }else{
           console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)');
        }
      }
    );
  }
}
