import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PatientService } from '../../../_services/patient.service';
import { Pacient } from '../../../_models/pacient';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PacientEditComponent } from '../pacient-edit/pacient-edit.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MedicationPlanComponent } from 'src/app/patient/medication-plan/medication-plan.component';


@Component({
  selector: 'app-pacients-table',
  templateUrl: './pacients-table.component.html',
  styleUrls: ['./pacients-table.component.css']
})
export class PacientsTableComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'gender', 'address', 'medical record', 'caregiver', 'medicalPlan', 'actions'];
  dataSource: MatTableDataSource<Pacient>;
  pacients: Pacient[] = [];
  bsModalRef: BsModalRef;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private patientService: PatientService,
    private modalService: BsModalService,
    public dialog: MatDialog,) {

    this.fetchData();
  }

  private fetchData() {
    this.patientService.getAll().subscribe(data => {
      this.pacients = data;
      console.log(this.pacients);
      this.dataSource.data = this.pacients;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource([]);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



  public openModal(template: TemplateRef<any>) {
    this.bsModalRef = this.modalService.show(template);
  }

  onUpdatePatient(row) {
    
    const initialState = {
      username: '',
      password: '',
      patient: this.pacients.find((pacient) => pacient.id === row.id),
      editPatient: true
    };

    this.bsModalRef = this.modalService.show(PacientEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  
  }

  onAddPatient(){
    const initialState = {
      username: '',
      password: '',
      patient: {},
      editPatient: false
    };

    this.bsModalRef = this.modalService.show(PacientEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  }


  onDeleteRow(row) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent,{width:'350px', data: 'Are you sure you want to delete '+ row.name+'?'});

    dialogRef.afterClosed().subscribe(
      result => {
        if(result){
          console.log('User confirmed:', result),
          this.patientService.deletePatient(row.id).subscribe(res=>{
            this.fetchData();
          });
    
        }else{
           console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)');
        }
      }
    );
  }

  
  onOpenMedicationPlan(row) {

    const initialState = {
      currentPatient : row,
      openInModal: true
    };

    this.bsModalRef = this.modalService.show(MedicationPlanComponent, { initialState });
   // this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());

  }
}
