import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../_models/user';
import { Role } from '../_models/role';
import { DoctorService } from '../_services/doctor.service';
import { Doctor } from '../_models/doctor';
import { Pacient } from '../_models/pacient';
import { Caregiver } from '../_models/caregiver';
import { PatientService } from '../_services/patient.service';
import { CaregiverService } from '../_services/caregiver.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private currentDoctorSubject: BehaviorSubject<Doctor>;
  public doctor: Observable<Doctor>;
  private patientSubject: BehaviorSubject<Pacient>;
  public patient: Observable<Pacient>;
  private caregiverSubject: BehaviorSubject<Caregiver>;
  public caregiver: Observable<Caregiver>;

  

  readyToRender: boolean;
  user: User;
  name: String;

  constructor(private doctorService: DoctorService,
    private patientService: PatientService,
    private caregiverService: CaregiverService) {

    this.currentDoctorSubject = new BehaviorSubject<Doctor>(new Doctor());
    this.doctor = this.currentDoctorSubject.asObservable();
    this.patientSubject = new BehaviorSubject<Pacient>(new Pacient(null, null, null, null, null, null));
    this.patient = this.patientSubject.asObservable();
    this.caregiverSubject = new BehaviorSubject<Caregiver>(new Caregiver(null, null, null, null, null));
    this.caregiver = this.caregiverSubject.asObservable();
  }
  ngOnInit() {
    this.computeDoctor();
  }

  public get currentUserValue(): Doctor {
    return this.currentDoctorSubject.value;
  }

  computeDoctor() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    console.log("Current doctor: " + this.user);
    if (this.user.role == Role.Doctor) {
      this.doctorService.getMyProfile(this.user).subscribe(res => {
        this.currentDoctorSubject.next(res);
        this.name = res.name;
        this.readyToRender = true;
        
      });
    } else {
      if (this.user.role == Role.Patient) {
        this.patientService.getMyProfile(this.user).subscribe(res => {
          this.patientSubject.next(res);
          this.name = res.name;
          this.readyToRender = true;
        });
      } else {
        if (this.user.role == Role.Caregiver) {
          this.caregiverService.getMyProfile(this.user).subscribe(res => {
            this.caregiverSubject.next(res);
            this.name = res.name;
            this.readyToRender = true;
          });
        }
      }
    }
  }

}
