import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { Intake } from 'src/app/_models/intake';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Intakeservice } from 'src/app/_services/intake.service';
import { PatientComponent } from '../patient.component';
import { Pacient } from 'src/app/_models/pacient';

@Component({
  selector: 'app-medication-plan',
  templateUrl: './medication-plan.component.html',
  styleUrls: ['./medication-plan.component.css']
})
export class MedicationPlanComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'description', 'start date', 'end date'];
  dataSource: MatTableDataSource<Intake>;
  intakes: Intake[] = [];
  bsModalRef: BsModalRef;

  
  @Input('patient') currentPatient: Pacient;
  openedInModal: boolean = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private intakeService: Intakeservice,
    private modalService: BsModalService,
    public dialog: MatDialog,) {

  
  }

  
  ngOnInit() {
    this.dataSource = new MatTableDataSource([]);
    this.fetchData();
  }

  private fetchData() {
    this.intakeService.getMyIntakes(this.currentPatient.id).subscribe(data => {
      this.intakes = data;
      this.dataSource.data = this.intakes;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onUpdateMedication(row) {
    
    const initialState = {
      medication: this.intakes.find((pacient) => pacient.id === row.id),
      editMedication: true
    };

   // this.bsModalRef = this.modalService.show(MedicationEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  
  }

  onAddPatient(){
    
    const initialState = {
      medication:{},
      editMedication: false
    };

   // this.bsModalRef = this.modalService.show(MedicationEditComponent, { initialState });
    this.bsModalRef.content.addEmitter.subscribe(() => this.fetchData());
  }


  onDeleteRow(row) {
  //  const dialogRef = this.dialog.open(ConfirmationDialogComponent,{width:'350px', data: 'Are you sure you want to delete '+ row.name+'?'});
/*
    dialogRef.afterClosed().subscribe(
      result => {
        if(result){
          console.log('User confirmed:', result),
          this.medicationService.deleteMedication(row.id).subscribe(res=>{
            this.fetchData();
          });
    
        }else{
           console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)');
        }
      }
    );
    */
  }
}
