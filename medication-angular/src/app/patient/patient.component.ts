import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Pacient } from '../_models/pacient';
import { PatientService } from '../_services/patient.service';
import { User } from '../_models/user';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  private readyToRender: boolean = false;
  private currentDoctorSubject: BehaviorSubject<Pacient>;
  public patient: Observable<Pacient>;


  constructor(private patientService: PatientService, ) {
    this.currentDoctorSubject = new BehaviorSubject<Pacient>(new Pacient(null,null,null,null,null,null));
    this.patient = this.currentDoctorSubject.asObservable();
  }
  ngOnInit() {
    this.computeDoctor();
  }

  public get currentUserValue(): Pacient {
    return this.currentDoctorSubject.value;
  }

  computeDoctor() {
    let user: User = JSON.parse(localStorage.getItem('currentUser'));
    console.log("Current doctor: " + user);
    this.patientService.getMyProfile(user).subscribe(res => {
      this.currentDoctorSubject.next(res);
      this.readyToRender = true;
    });
  }

}
